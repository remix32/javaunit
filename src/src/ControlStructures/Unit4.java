package ControlStructures;

public class Unit4
{
    public static void main(String[] args) {
        for (int i = 0 ; i < 150 ; i ++) {
            System.out.println(i);
            if ( i== 99) break;

        }
        System.out.println();
        for (int i = 0 ; i < 150 ; i ++) {
            System.out.println(i);
            if ( i== 99) return;

        }
    }
}
