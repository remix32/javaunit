package Operators;

public class Unit6
{
    public static void main(String[] args) {
        Dog scruffy = new Dog();
        scruffy.name = "sharik";
        scruffy.says = "ay ay";
        scruffy.print();
        Dog butch = new Dog();
        butch.name = "butch";
        butch.says = "ph ph";
        butch.print();

        System.out.println("Сравнение");
        butch=scruffy;
        System.out.println("==" + (butch == scruffy));
        System.out.println("equals " + butch.equals(scruffy));
        System.out.println("Butch");
        butch.print();
        System.out.println("Scruffy");
        scruffy.print();
    }
}
