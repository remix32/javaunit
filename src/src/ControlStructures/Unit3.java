package ControlStructures;


public class Unit3
{
    static int test (int testval , int target , int begin , int end) {
        if (begin <= testval && testval <= end) {
            System.out.println("Значение подходят");
            if (testval > target) {
                return +1;
            } else if (testval < target) {
                return -1;
            } else return 0;

        } else
            {
                System.out.println("Значения не подходят");
                return Integer.MAX_VALUE;
            }
    }

    public static void main(String[] args)
    {
        System.out.println(Unit3.test(10,5,3,12));
        System.out.println(Unit3.test(5,51,7,11));
    }
}
