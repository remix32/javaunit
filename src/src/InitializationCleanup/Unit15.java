package InitializationCleanup;

class Tester {
    String s;
    {
        s = "Initializing string in Tester";
        System.out.println(s);
    }
    Tester() {
        System.out.println("Tester()");
    }
}

public class Unit15 {
    public static void main(String[] args) {
        Tester t = new Tester();
    }
}
