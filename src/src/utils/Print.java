package utils;

import java.io.PrintStream;

public class Print {

    public static void print (Object object) {
        System.out.print(object);
    }

    public static void println(Object object) {
        System.out.println(object);
    }

    public static PrintStream printf (String format,Object...arg) {
        return System.out.printf(format,arg);
    }
}
