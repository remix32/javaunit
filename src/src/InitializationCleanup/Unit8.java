package InitializationCleanup;

class Doc {
    void intubate() {
        System.out.println("prepare patient");
        laryngoscopy();
        this.laryngoscopy();
    }
    void laryngoscopy() {
        System.out.println("use laryngoscope");
    }
}

public class Unit8 {
    public static void main(String[] args) {
        Doc d = new Doc();
        d.intubate();
    }
}
