package ControlStructures;

class Switch {

    public void switchTest (int n ) {
        for (int i = 0 ; i <= n ; i++) {
            switch (i) {
                case 1: System.out.println("Первый");break;
                case 2: System.out.println("Второй");break;
                case 3: System.out.println("Третий");break;
                case 4: System.out.println("Четвертый");break;
                case 5: System.out.println("Пятый");break;
                default: System.out.println("default");break;
            }

        }
    }
}

public class Unit6
{
    public static void main(String[] args) {
        Switch s = new Switch();
        s.switchTest(5);
    }
}
