package Operators;

class Dog
{
    String name;
    String says;
    void print () {
        System.out.println(name + " " + says);
    }
}

public class Unit5
{
    public static void main(String[] args) {
        Dog spot = new Dog();
        Dog scruffy = new Dog();
        spot.name = "Reks";
        spot.says = "gav gav";
        spot.print();
        scruffy.name = "sharik";
        scruffy.says = "ay ay";
        scruffy.print();

    }
}
