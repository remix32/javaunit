package Operators;

class Print
{
    static void print (String s)
    {
        System.out.println(s);
    }

   static void println (String s)
    {
        System.out.print(s);
    }
}

public class Unit1
{
    public static void main(String[] args)
    {
        Print pr = new Print();
        pr.print("Короткий вывод");
        System.out.println("Длинный вывод");
    }
}
