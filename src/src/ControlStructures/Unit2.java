package ControlStructures;

import java.util.Random;

public class Unit2
{
    public static void main(String[] args) {
        Random rnd = new Random();
        for (int i = 0 ; i< 25; i++) {
            int x = rnd.nextInt(10);
            int y = rnd.nextInt(12);
            if (x<y) System.out.println("x<y");
            else if (x>y) System.out.println("x>y");
            else System.out.println("x=y");
        }
    }
}
