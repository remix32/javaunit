package Operators;

class Letter {
    char c;
}

public class Unit3 {
    public static void f(Letter f) {
        f.c = 'l';
    }

    public static void main(String[] args) {
        Letter l = new Letter();
        l.c = 'a';
        System.out.println("L=" +l.c);
        f(l);
        System.out.println("L=" +l.c);
    }
}
