package Operators;

class Speedometr {

    public static float currentSpeed(float speed,float distance)
    {
        return distance > 0 ? distance / speed : -1;
    }

}

public class Unit4
{
    public static void main(String[] args)
    {
        float d = 565.3f;
        float t = 3.6f;
        System.out.println("Distance: " + d);
        System.out.println("Time: " + t);
        float v = Speedometr.currentSpeed(d, t);
        System.out.println("currentSpeed: " + v);
    }
}
