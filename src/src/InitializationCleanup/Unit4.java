package InitializationCleanup;

class Construct1 {
    Construct1 () {
        System.out.println("Hello");
    }
    Construct1 (String s) {
        System.out.println("Hello " + s);
    }
}

public class Unit4 {
    public static void main(String[] args) {
        Construct1 c1 = new Construct1();
        Construct1 c2 = new Construct1("Word");
    }
}
